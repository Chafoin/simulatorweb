const Pool = require('pg').Pool
const pool = new Pool({
  user: 'tp',
  host: 'localhost',
  database: 'simulator',
  password: 'tp',
  port: 5432,
})

function showError(response,e){
    response.status(500).json(e)
    console.log("error",e)
}


const getAll = (request, response) => {
    const table = request.params.table
    
    pool.query('SELECT * FROM public."'+request.params.table+'"',(error, results) => {
      //response.setRequestHeader("Content-Type","application/json");
      if(!results){
        response.status(200).json([])
        return
      }
      if (error) {
        showError(response,error)
      }
      response.status(200).json(results.rows)
    })
  }
  
  const getOne = (request, response) => {
    const id = parseInt(request.params.id)
  
    pool.query('SELECT * FROM public."'+request.params.table+'" WHERE id = $1', [id], (error, results) => {
      if (error) {
        showError(response,error)
      }
      response.status(200).json(results.rows)
    })
  }
  
  const insert = (request, response) => {
    var keys=""
    var values=""
    t = paramBodyInsertFormat(request.body)
    keys = t[0]
    values = t[1]
    var query='INSERT INTO public."'+request.params.table+'" ('+keys+') VALUES ('+values+') RETURNING id'
    
    
    pool.query(query, (error, results) => {
        if (error) {
            showError(response,error)
        }
        response.status(200).send(results.rows[0].id+"")
    })
  }
  
  const update = (request, response) => {
    var set= paramBodyUpdateFormat(request.body)
    pool.query(
        'UPDATE public."'+request.params.table+' SET '+set+" where id = "+request.params.id,
        (error, results) => {
          if (error) {
              showError(response,error)
          }
        response.status(200).send(`ok`)
        }
    )
  }
  const fireFormatController = (request,response)=>{
    const table = request.params.table
    pool.query('SELECT count(*) as c FROM fire',(error, results) => {
      console.log(results.rows[0].c)
      if(results.rows[0].c*1){
        pool.query('SELECT * FROM fire where last_modified>to_timestamp('+request.params.timestamp+")  order by id",(error, results) => {
          if(!results){
            response.status(200).json([])
            return
          }
          if (error) {
            showError(response,error)
          }
          response.status(200).json(results.rows.map(f=>{
            return {
              id:f.idSensor,
              i:f.intensity
            }
          }))
        })
      }else{
        response.status(200).json([{"id":"-1","i":"-1"}])
      }
    })
    
  }
    
  module.exports = {
    getAll,
    getOne,
    insert,
    update,
    fireFormatController,
  }
  function paramBodyInsertFormat(body){
    var keys=""
    var values=""
    Object.keys(body).forEach((k,i)=>{
        var isNumber = typeof body[k] == "number"
        keys+=(k
            +(i+1!=Object.keys(body).length?",":""))
    
        values+=((isNumber?"":"'")
            +body[k]
            +(isNumber?"":"'")
            +(i+1!=Object.keys(body).length?",":""))
    })
    return [keys,values]
  }
  function paramBodyUpdateFormat(body){
    var s=""
    Object.keys(body).forEach((k,i)=>{
        if(k!='id'){
            var isNumber = typeof body[k] == "number"
            s+=(k
                +" = "
                +(isNumber?"":"'")
                +body[k]
                +(isNumber?"":"'")
                +(i+1!=Object.keys(body).length?",":"")
            )
        }
    })
    return s
  }