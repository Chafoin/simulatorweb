async function uhttp(method,url,body,requestHeader) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        
        xhr.setRequestHeader("Content-Type","application/json");
        
        xhr.onload = function () {
            
            var status = xhr.status;
            if (status == 200) {
                try{
                    resolve(JSON.parse(xhr.response));
                }catch(e){
                    resolve(xhr.response);
                }
            } else {
                reject(status);
            }
        };
        xhr.send(JSON.stringify(body));
    });
}