var app = new Vue({
    el:"#vue",
    data:{
        map:null,
        initLontitude:[45.760463, 4.863472],
        initZoom:13,
        sensors:[],
        fires:[],
        fireTruck:[],
        filterFire:false,
        filterTruck:false
    },
    methods:{
        removeFire:function(){
            this.sensors.forEach(s=>{
                if(s.marker)
                    s.marker.remove()
            })
        },
        drawFire:async function(){
            this.removeFire()
            
            var geojsonMarkerOptions = {
                radius: 8,
                fillColor: "#00FF00",
                color: "#000",
                weight: 1,
                opacity: 1,
                fillOpacity: 0.8
            };

            
            this.sensors.forEach(s=>{
                var f  = this.fires.find(f=>{
                    return f.idSensor==s.id
                })
                geojsonMarkerOptions.fillColor= this.getColor(f?f.intensity:0)
                
                s.marker = L.circleMarker([s.latitude,s.longitude], geojsonMarkerOptions).addTo(this.map).bindPopup(`X:${s.longitude} Y:${s.latitude}<br>Intensity:${f?f.intensity:0}`)
            })
        },
        getFires:async function(){
            this.fires = await uhttp('get','db/activefire')
        },
        getColor(intensity){
            if(!intensity)
                return "#FFF"
            return "rgb(255,"+(10-intensity)*25+",0)"
        },
        removeTruck:function(){
            this.fireTruck.forEach(ft=>{
                if(ft.marker)
                    ft.marker.remove()
            })
        },
        getViewTruck:async function(){
            this.removeTruck()
            this.fireTruck= await uhttp('GET','db/fireTruck')
               
        },
        drawFireTruck:function(){
            
            var firetruck = L.icon({
                iconUrl: 'img/firetruck.png',
                //shadowUrl: 'leaf-shadow.png',
            
                iconSize:     [30, 15], // size of the icon
                //shadowSize:   [50, 64], // size of the shadow
                iconAnchor:   [15, 6], // point of the icon which will correspond to marker's location
                //shadowAnchor: [4, 62],  // the same for the shadow
                popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
            });
            this.fireTruck.forEach(tr=>{
                tr.marker = L.marker([tr.latitude,tr.longitude], {icon: firetruck}).addTo(this.map).bindPopup("Camion "+tr.id);
                //tr.marker = L.polyline([tr.longitude,tr.latitude], {color: 'blue'}).addTo(this.map).bindPopup("Camion "+tr.id);
            })
        },
        refresh:async function(){
            await this.getFires()
            if(!this.filterFire)
                this.drawFire()
            await this.getViewTruck()
            if(!this.filterTruck)
                this.drawFireTruck()
        }

    },
    watch:{
        filterFire:function(){
            if(this.filterFire)
                this.removeFire()
            else
                this.drawFire()
        },
        filterTruck:function(){
            if(this.filterTruck)
                this.removeTruck()
            else
                this.drawFireTruck()
        }
    },
    mounted:async function(){
        this.map = L.map('map').setView(this.initLontitude, this.initZoom);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        this.sensors = await uhttp("GET","db/fireSensor")
        this.refresh()
        setInterval(async ()=>{
            app.refresh()
        },3000)
        

    }
})