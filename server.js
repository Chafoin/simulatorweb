const express = require('express')

const db = require('./module/query')
const fs = require('fs')
const path = require('path')
const app = express()
const port = 3000



app.use(express.static('public'));
app.use(express.json())

app.get('/', (req, res) => {
    res.sendFile(path.resolve('index.html'));
  })
app.get('/db/:table', db.getAll)
app.get('/db/:table/:id', db.getOne)
app.post('/db/:table', db.insert)
app.put('/db/:table/:id', db.update)

app.get('/fire/:timestamp', db.fireFormatController)




app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})